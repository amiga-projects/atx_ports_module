# Amiga 4000 Tower ATX Ports Module

## Problem

To this day, the Amiga 4000 Tower is a fascinating machine.

Unfortunately, it can be taken for granted that there are more working Amiga
4000T mainboards than (intact) original cases out there.

As an owner of a "naked" mainboard you can therefore choose between three main
possibilities:
 
 * Wait, until an empty case is for sale somewhere and be the lucky buyer.
 * Use a vintage PC AT tower case. These are still available, but there are
   virtually no pretty ones among those.
 * Use a modern, quiet E-ATX case with cable management and other modern features.
 
Choosing the latter leaves you with the question of where to put the ports
(mouse joystick, serial, parallel.) The ports-module is taylored for the original
case.

## Solution

This project repository comes to the rescue.

It contains the files necessary to build a replacement ports module that fits
the ATX panel opening in your E-ATX case.

![ATX Ports Module (front view)](Image/ATX-Ports-Module_Front.jpg)
![ATX Ports Module (rear view)](Image/ATX-Ports-Module_Back.jpg)
![ATX Ports Module (bottom view)](Image/ATX-Ports-Module_Bottom.jpg)
![ATX Ports Module (in use)](Image/ATX-Ports-Module_InUse.jpg)

While the SCSI terminator that is part of the original ports module has been
omitted, this incarnation of the module is very compact and adds (optional)
overvoltage/ESD protection to the ports to shield the valuable chips in your
Amiga.

## License and Disclaimer

**BEFORE** going ahead and building one or more of these modules, please
consider the following:

***This project is shared with you under a Creative Commons Attribution-ShareAlike 4.0
license.***
You may produce, distribute, and use this project freely ***as long as you***:
 * Leave **all attributions and copyright notices** which are present in this
   repository, within the design files, and on the final product (silkscreen)
   intact, and only add to them
 * **Share** any modifications and improvements under the same license
 * **Indicate** that you modified the product and what modifications you made
 
The exact license terms can be found in the [LICENSE](LICENSE) file.

***DISCLAIMER***

This is a hobbyist project.

Although it works for me, there's no guarantee that it will work for you, your
neighbor, or your buddy.

Use it at your own risk!

I, TORSTEN KURBAD, EXPRESSLY DISCLAIM ANY RESPONSIBILITY FOR ANY DAMAGE, INJURY,
HARM, COST, EXPENSE, OR LIABILITY ARISING OUT OF OR RELATED TO YOUR USE OR MISUSE
OF THE INFORMATION IN THIS REPOSITORY. THE INFORMATION IS PROVIDED ON AN AS-IS
BASIS AND WITHOUT WARRANTY OF ANY KIND, WHETHER EXPRESS, IMPLIED, OR STATUTORY,
INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PUPOSE, TITLE, ACCURACY, NON-INFRINGEMENT, OR QUALITY.

All Trademarks used are the property of their legal owner and are used solely
for descriptive purposes herein.

## BOM

1x PCB ATX Ports Module Carrier  
1x PCB ATX Ports Module Riser  
2x 2x20 pin female pinsocket, pitch 2.54 mm  
2x 1x7 pin male pinheader, pitch 2.54 mm  
2x 1x5 pin male pinheader, pitch 2.54 mm  
3x electrolytic (polymer) SMD capacitor 4.7µF 16V  
3x ceramic 1206 SMD capacitor 100nF  
2x male DSub-9 connector for PCB mounting at 180° angle  
1x male DSub-25 connector for PCB mounting at 180° angle  
1x female DSub-25 connector for PCB mountint at 180° angle  
*optional:*  
30x TVS diode 5V bidirectional SOD-323 (e.g. Bourns CSOD323-T05**C**)  
10x TVS diode 15~24V bidirectional SOD-323 (e.g. Bourns CSOD323-T24**C**)  
2x  TVS diode 12V bidirectional SOD-323 (e.g. Bourns CSOD323-T12**C**)  

## Build Instruction

To build such a module, you need a good soldering iron and magnification.

Begin by populating all SMD components on the riser PCB. Start with the small
ones, i.e. the TVS diodes, then ceramic capacitors, then electrolytic capacitors.

![ATX Ports Module - Step 1](Image/ATX-Ports-Module_Build_01.jpg)
![ATX Ports Module - Step 2](Image/ATX-Ports-Module_Build_02.jpg)

Now populate the rear female header (CN150) on the carrier PCB (populate from
below, silk screen is on top.) For CN160, only use a very small amount of solder
to hold it in place at two or three pins:

![ATX Ports Module - Step 3](Image/ATX-Ports-Module_Build_03.jpg)
![ATX Ports Module - Step 4](Image/ATX-Ports-Module_Build_04.jpg)

Now put the riser PCB vertically between the two pin rows of CN160 and fixate it
at a few pins:

![ATX Ports Module - Step 5](Image/ATX-Ports-Module_Build_05.jpg)
![ATX Ports Module - Step 6](Image/ATX-Ports-Module_Build_06.jpg)

Prepare the 5- and 7-pin male headers as follows. File the plastic down on one
side so they fit at the sides of CN160:

![ATX Ports Module - Step 7](Image/ATX-Ports-Module_Build_07.jpg)

Start with one of the 7-pin and one of the 5-pin headers. Stick them through the
carrier from below, making sure that there's some room between the PCB and the
plastic part of the header. Fixate one of the pins on the riser PCB with a little
solder:

![ATX Ports Module - Step 8](Image/ATX-Ports-Module_Build_08.jpg)

Now solder all the pins that are already in place. Be careful to create a good
connection on the carrier as well as on the riser PCB. Afterwards, remove the
downward end of the pinheaders (with the plastic) using a clipper. Then, solder
in the remaining two pinheaders, following the same scheme, and clip them.

The result should look somewhat like this:

![ATX Ports Module - Step 9](Image/ATX-Ports-Module_Build_09.jpg)
![ATX Ports Module - Step 10](Image/ATX-Ports-Module_Build_10.jpg)

**Carefully verify** that there are no bridges. If necessary, remove them with
solder wick!

Insert the DSub connectors from the front of the riser. Lay the module
flat on your workbench and with light pressure, fix the connectors in a few spots.

Finally, solder all pins:

![ATX Ports Module - Step 11](Image/ATX-Ports-Module_Build_11.jpg)

After a thorough final inspection, plug the module carefully on the pinheaders
of your A4000T mainboard. Make sure that you leave no pins out.

The module should sit firmly (on purpose.) The holes in the carrier board align
with two of the mounting holes of the mainboard. You can use those to fix the
module in place using standoffs.

To test the assembly, I recommend the latest release of Keir Frasers
[Amiga Test Kit](https://github.com/keirf/amiga-stuff/tags), specifically the
'Controller Ports' and 'Serial/Parallel' tests.

Have fun!

## Copyright

This work is © 2023 by Torsten 'torsti76' Kurbad
